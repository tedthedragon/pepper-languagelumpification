
// ===================================================================
// DAIMAOU Industries "Mauler" Gretchenfrage Cannon
// ===================================================================
// After the obvious battlefield success of the BFG9000 prototype, the race began to produce a further miniaturized and streamlined successor for mass production.
// First to the finish line was the Mauler, a brute force hackjob designed in a single afternoon. 
// It's a testament to the skill of its engineers that it works at all, inefficient as it is.
// While its bombardment capability is far weaker than the full BFG, and all energy production capabilities were discarded, 
// they did hack in a wide-area plasma dispersion function that will annihilate anything in close quarters.
// Its poor effective range and high energy demands leave it unpopular among conventional militaries, 
// but rumors have surfaced of a squad of mercenary cultists utilizing it along with a BFG to great effect in retaking and defending dense urban centers.


const HDLD_MLR="mlr";


class HDMauler:HDCellWeapon{
	default{
		//$Category "Weapons/Hideous Destructor"
		//$Title "Mauler"
		//$Sprite "BFUGA0"

		weapon.selectionorder 81;
		weapon.slotnumber 6;
		weapon.slotpriority 0.8;
		weapon.kickback 150;
		scale 0.9;
		hdweapon.barrelsize 30,1.7,3.5;
		hdweapon.refid HDLD_MLR;
		tag "$PEPPER_TAG_MAULER";
		
		hdweapon.loadoutcodes "
            \cualt - 0/1, whether to start in torpedo mode";
	}
	override string pickupmessage() {return Stringtable.Localize("$PEPPER_PICKUP_MAULER");}
	override string getobituary(actor victim,actor inflictor,name mod,bool playerattack){
		if(bplayingid)return Stringtable.Localize("$PEPPER_OBITUARY_MAULER_ID");
		return Stringtable.Localize("$PEPPER_OBITUARY_MAULER");
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	//BFG9k.Spark(self,4);
	//BFG9k.Spark(self,4,gunheight()-2);
	static void Spark(actor caller,int sparks=1,double sparkheight=10){
		actor a;vector3 spot;
		vector3 origin=caller.pos+(0,0,sparkheight);
		double spp;double spa;
		for(int i=0;i<sparks;i++){
			spp=caller.pitch+frandom(-20,20);
			spa=caller.angle+frandom(-20,20);
			spot=random(32,57)*(cos(spp)*cos(spa),cos(spp)*sin(spa),-sin(spp));
			a=caller.spawn("BFGSpark",origin+spot,ALLOW_REPLACE);
			a.vel+=caller.vel*0.9-spot*0.03;
		}
	}
	actor ShootBall(actor inflictor,actor source){
		inflictor.A_StartSound("weapons/MaulerFwoosh",CHAN_WEAPON,CHANF_OVERLAP);
		weaponstatus[MLRS_BATTERY]-=6;
		weaponstatus[0]&=~MLRF_CRITICAL;

		vector3 ballvel=(cos(inflictor.pitch)*(cos(inflictor.angle),sin(inflictor.angle)),-sin(inflictor.pitch));

		vector3 spawnpos=(inflictor.pos.xy,inflictor.pos.z+inflictor.height*0.8)+ballvel*6;
		if(inflictor.viewpos)spawnpos+=inflictor.viewpos.offset;

		let bbb=spawn("MaulerBallTail",spawnpos);
		if(bbb){
			bbb.target=source;
			bbb.pitch=inflictor.pitch;
			bbb.angle=inflictor.angle;
			bbb.vel=inflictor.vel+ballvel*4.;
		}
		bbb=spawn("MaulerBall",spawnpos);
		if(bbb){
			bbb.target=source;
			bbb.master=source;
			bbb.pitch=inflictor.pitch;
			bbb.angle=inflictor.angle;
			bbb.vel=inflictor.vel+ballvel*12.;
		}
		return bbb;
	}
	override void ForceBasicAmmo(){
		ForceOneBasicAmmo("HDBattery",1);
	}
	override double gunmass(){
		return 15
		+(weaponstatus[MLRS_BATTERY]>=0?1:0);
	}
	override double weaponbulk(){
		return 200
			+(weaponstatus[MLRS_BATTERY]>=0?ENC_BATTERY_LOADED:0)
		;
	}
	override string,double getpickupsprite(){return "MLRPA0",1.4;}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			sb.drawbattery(-54,-4,sb.DI_SCREEN_CENTER_BOTTOM,reloadorder:true);
			sb.drawnum(hpl.countinv("HDBattery"),-46,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		int bffb=hdw.weaponstatus[MLRS_BATTERY];
		if(bffb>0)sb.drawwepnum(bffb,20);
		else if(!bffb)sb.drawstring(
			sb.mamountfont,"000000",
			(-16,-7),sb.DI_TEXT_ALIGN_RIGHT|sb.DI_TRANSLATABLE|sb.DI_SCREEN_CENTER_BOTTOM,
			Font.CR_DARKGRAY
		);
		if(hdw.weaponstatus[0]&MLRF_ALT){
			sb.drawrect(-18,-14,2,4); //right side
            //sb.drawrect(-21,-14,4,2); //tail
			sb.drawrect(-21,-15,3,6); //big orb
			sb.drawrect(-23,-14,2,4); //left side
		}else{
            sb.drawrect(-40,-14,8,1);
            sb.drawrect(-40,-12,8,1); 
            sb.drawrect(-40,-10,8,1);
        }
	}
	
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		sb.SetClipRect(
			-16+bob.x,-4+bob.y,32,16,
			sb.DI_SCREEN_CENTER
		);
		vector2 bobb=bob*1.2;
		sb.drawimage(
			"tbfrntsit",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"tbbaksit",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9
		);
	}
	
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTFIRE.."  Switch to "..(weaponstatus[0]&MLRF_ALT?"dispersion":"torpedo").." mode\n"
		..WEPHELP_RELOADRELOAD
		..WEPHELP_UNLOADUNLOAD
		;
	}
	override void consolidate(){
		CheckBFGCharge(MLRS_BATTERY);
		//CheckBFGCharge(MLRS_CHARGE);
	}
	
	
    static void MaulerZap(
		actor caller,
		double zoffset=32,
		bool alt=true,
		int battery=20
	){
		//determine angle
		double shootangle=caller.angle;
		double shootpitch=caller.pitch;
		vector3 shootpos=(0,0,zoffset);
		let hdp=hdplayerpawn(caller);
		if(hdp){
			shootangle=hdp.gunangle;
			shootpitch=hdp.gunpitch;
			shootpos=hdp.gunpos;
		}
		if(alt){
			shootangle+=frandom(-6.0,6.0);
			shootpitch+=frandom(-3.9,3.3);
		}

		//create the line
		flinetracedata tlt;
		caller.linetrace(
			shootangle,
			8000+200*battery,
			shootpitch,
			flags:TRF_NOSKY|TRF_ABSOFFSET,
			offsetz:shootpos.z,
			offsetforward:shootpos.x,
			offsetside:shootpos.y,
			data:tlt
		);


		if(
			tlt.hittype==Trace_HitNone
			||(
				tlt.hitline&&(
					tlt.hitline.special==Line_Horizon
					||(
						tlt.linepart==2
						&&tlt.hitsector.gettexture(0)==skyflatnum
					)||(
						tlt.linepart==1
						&&tlt.hitline.sidedef[1]
						&&hdmath.oppositesector(tlt.hitline,tlt.hitsector).gettexture(0)==skyflatnum
					)
				)
			)
		)return;

		//alt does a totally different thing
			if(tlt.hittype==Trace_HitNone||tlt.distance>2000)return;
			actor bbb=spawn("MaulerSpotFlash",tlt.hitlocation-tlt.hitdir,ALLOW_REPLACE);
			bbb.setz(clamp(bbb.pos.z,bbb.floorz,bbb.ceilingz-8));
			if(!random(0,3))(maulerlingeringthunder.zap(bbb,bbb,caller,40,true));
			maulerspotflash(bbb).impactdistance=tlt.distance-16*battery;
			bbb.angle=caller.angle;
			bbb.A_SprayDecal("Scorch",12);
			bbb.pitch=caller.pitch;
			bbb.target=caller;
			bbb.tracer=tlt.hitactor; //damage inflicted on the puff's end
			if(tlt.hitline)doordestroyer.CheckDirtyWindowBreak(tlt.hitline,0.005*battery,tlt.hitlocation);
			return;
		

		int basedmg=int(max(0,20-tlt.distance*(1./50.)));
		int dmgflags=caller&&caller.player?DMG_PLAYERATTACK:0; //don't know why the player damagemobj doesn't work

		if(tlt.hitline)doordestroyer.CheckDirtyWindowBreak(tlt.hitline,0.003*basedmg,tlt.hitlocation);

		//wet actor
		if(
			tlt.hitactor
		){
			actor hitactor=tlt.hitactor;
			if(hitactor.bloodtype=="ShieldNotBlood"){
				hitactor.damagemobj(null,caller,random(1,(battery<<2)),"Balefire",dmgflags);
			}else if(
				hitactor.bnodamage
				||hitactor.bnoblood
				||hitactor.bloodtype=="NotQuiteBloodSplat"
				||random(0,7)
				||hitactor.countinv("ImmunityToFire")
				||HDWoundFixer.CheckCovered(hitactor,true)
			){
				//dry actor - ping damage and continue
				if(!random(0,5))(maulerlingeringthunder.zap(hitactor,hitactor,caller,40,true));
				hdf.give(hitactor,"Heat",(basedmg>>1));
				hitactor.damagemobj(null,caller,1,"electrical",dmgflags);
			}else{
				//wet actor
				if(!random(0,7))(maulerlingeringthunder.zap(hitactor,hitactor,caller,(basedmg<<1),true));
				hdf.give(hitactor,"Heat",(basedmg<<1));
				hitactor.damagemobj(null,caller,basedmg,"electrical",dmgflags);
				actor sss=spawn("HDGunsmoke",tlt.hitlocation,ALLOW_REPLACE);
				sss.vel=(0,0,1)-tlt.hitdir;
				return;
			}
		}
	}
	
	action void A_MaulerZap(){
		int battery=invoker.weaponstatus[MLRS_BATTERY];
		if(battery<1){
			setweaponstate("nope");
			return;
		}

		//preliminary effects
		A_ZoomRecoil(0.99);
		A_StartSound("weapons/maulidle");
		if(countinv("IsMoving")>9)A_MuzzleClimb(frandom(-0.8,0.8),frandom(-0.8,0.8));

		//the actual call
		HDMauler.MaulerZap(
			self,
			gunheight(),
			true,
			battery
		);

		//aftereffects
			A_MuzzleClimb(
				frandom(0.025,0.1),frandom(-0.1,-0.2),
				frandom(0.05,0.15),frandom(-0.15,-0.3),
				frandom(0.02,0.06),frandom(-0.05,-0.15),
				frandom(0.005,0.015),frandom(-0.05,-0.1)
			);
		}
	
	states{
	altfire:
	firemode:
		#### A 1 offset(1,32) A_WeaponBusy();
		#### A 2 offset(2,32);
		#### A 1 offset(1,33) A_StartSound("weapons/plasswitch",8);
		#### A 2 offset(0,34);
		#### A 3 offset(-1,35);
		#### A 4 offset(-1,36);
		#### A 3 offset(-1,35);
		#### A 2 offset(0,34){
			invoker.weaponstatus[0]^=MLRF_ALT;
			A_SetHelpText();
		}
		#### A 1;
		#### A 1 offset(0,34);
		#### A 1 offset(1,33);
		goto nope;
	ready:
		P2SH A 1{
			if(invoker.weaponstatus[0]&MLRF_CRITICAL)setweaponstate("shoot");
			A_WeaponReady(WRF_ALL);
		}goto readyend;
	select0:
		P2SH A 0;
		goto select0bfg;
	deselect0:
        P2SH A 0;
		goto deselect0bfg;
	althold:
		stop;
	flash:
		#### A 3 bright{
			A_Light1();
			HDFlashAlpha(0,true);
		}
		#### A 2 bright{
			A_Light2();
			HDFlashAlpha(200);
		}
		#### A 2 bright HDFlashAlpha(128);
		goto lightdone;

	fire:
		#### A 0 {invoker.weaponstatus[MLRS_TIMER]=0;}
	hold:
        #### A 0 A_JumpIf(invoker.weaponstatus[MLRS_BATTERY]>0,1);
		goto nope;
		#### A 0 A_JumpIf(invoker.weaponstatus[0]&MLRF_ALT, "firetorp");
	firespray:
        #### B 0{
            for(int i=0;i<20;i++){
            if(invoker.weaponstatus[MLRS_BATTERY]>=1)
            {
                A_MaulerZap();
                if(!random(0,5)){
                    invoker.weaponstatus[MLRS_BATTERY]--;
                    }
                }
            }
            A_StartSound("weapons/maulsprayfire", CHAN_WEAPON, CHANF_OVERLAP);
        }
        #### BC 2;
        #### A 6;
        goto nope;
	unload:
		#### A 0{
			invoker.weaponstatus[0]|=MLRF_JUSTUNLOAD;
			if(invoker.weaponstatus[MLRS_BATTERY]>=0)
				return resolvestate("unmag");
			return resolvestate("nope");
		}goto magout;
	unmag:
		#### A 2 offset(0,33){
			A_SetCrosshair(21);
			A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
		}
		#### A 3 offset(0,35) A_StartSound("weapons/maulopen",8);
		#### A 6 offset(0,40) A_StartSound("weapons/maulload",8,CHANF_OVERLAP);
		#### A 0{
			int bat=invoker.weaponstatus[MLRS_BATTERY];
			A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
			if(
				(
					bat<0
				)||(
					!PressingUnload()&&!PressingReload()
				)
			)return resolvestate("dropmag");
			return resolvestate("pocketmag");
		}	
		
	firetorp:
		#### A 4{
			invoker.weaponstatus[MLRS_TIMER]++;
			if(invoker.weaponstatus[MLRS_TIMER]>4){
				invoker.weaponstatus[MLRS_TIMER]=0;
				
			}
			
			A_StartSound("weapons/maulcharge",CHAN_WEAPON);
			BFG9k.Spark(self,1,gunheight()-2);
			A_WeaponReady(WRF_NOFIRE);
		}
		#### A 0{
			if(invoker.weaponstatus[MLRS_BATTERY]>=6&&invoker.weaponstatus[MLRS_TIMER]==3){
			A_Refire("shoot");}
			else {A_Refire();}
		}
		goto ready;
	chargeend:
		#### A 1{
			BFG9k.Spark(self,1,gunheight()-2);
			A_StartSound("weapons/maulcharge",(invoker.weaponstatus[MLRS_TIMER]>6)?CHAN_AUTO:CHAN_WEAPON);
			A_WeaponReady(WRF_ALLOWRELOAD|WRF_NOFIRE|WRF_DISABLESWITCH);
			A_SetTics(max(1,6-int(invoker.weaponstatus[MLRS_TIMER]*0.3)));
			invoker.weaponstatus[MLRS_TIMER]++;
		}
		#### A 0{
			if(invoker.weaponstatus[MLRS_TIMER]>7)A_Refire("shoot");
			else A_Refire("chargeend");
		}goto ready;
	shoot:
		#### A 0{
			invoker.weaponstatus[MLRS_TIMER]=0;
			invoker.weaponstatus[0]|=MLRF_CRITICAL;
			invoker.weaponstatus[MLRS_CRITTIMER]=15;
			A_StartSound("weapons/MaulF",CHAN_WEAPON);
			hdmobai.frighten(self,512);
		}
		#### A 3{
			invoker.weaponstatus[MLRS_CRITTIMER]--;
			A_StartSound("weapons/maulcharge",random(9005,9007));
			BFG9k.Spark(self,1,gunheight()-2);
			if(invoker.weaponstatus[MLRS_CRITTIMER]<1){
				invoker.weaponstatus[MLRS_CRITTIMER]=0;
				player.setpsprite(PSP_WEAPON,invoker.findstate("reallyshoot"));
			}else if(invoker.weaponstatus[MLRS_CRITTIMER]<10)A_SetTics(2);
			else if(invoker.weaponstatus[MLRS_CRITTIMER]<5)A_SetTics(1);
		}wait;
	reallyshoot:
		#### A 8{
			A_AlertMonsters();
			hdmobai.frighten(self,1024);
		}
		#### A 2{
			A_ZoomRecoil(0.2);
			A_GunFlash();
			invoker.ShootBall(self,self);
		}
		#### BC 3 A_ChangeVelocity(-1,0,1.5,CVF_RELATIVE);
		#### A 6{
			A_MuzzleClimb(
				1,3,
				-frandom(0.4,0.6),-frandom(1.2,2.3),
				-frandom(0.9,1.4),-frandom(3.2,4.8),
				1,2
			);
			if(!random(0,10))DropInventory(invoker);
		}goto nope;
    

	dropmag:
		---- A 0{
			int bat=invoker.weaponstatus[MLRS_BATTERY];
			invoker.weaponstatus[MLRS_BATTERY]=-1;
			if(bat>=0){
				HDMagAmmo.SpawnMag(self,"HDBattery",bat);
			}
		}goto magout;

	pocketmag:
		---- A 0{
			int bat=invoker.weaponstatus[MLRS_BATTERY];
			invoker.weaponstatus[MLRS_BATTERY]=-1;
			if(bat>=0){
				HDMagAmmo.GiveMag(self,"HDBattery",bat);
			}
		}
		#### A 8 offset(0,43) A_StartSound("weapons/pocket",9);
		#### A 8 offset(0,42) A_StartSound("weapons/pocket",9,CHANF_OVERLAP);
		goto magout;

	magout:
		---- A 0 A_JumpIf(invoker.weaponstatus[0]&MLRF_JUSTUNLOAD,"reload3");
		goto loadmag;

	reload:
		#### A 0{
			invoker.weaponstatus[0]&=~MLRF_JUSTUNLOAD;
			if(
				invoker.weaponstatus[MLRS_BATTERY]<20
				&&countinv("HDBattery")
			)setweaponstate("unmag");
		}goto nope;

	loadmag:
		#### A 2 offset(0,43){
			A_StartSound("weapons/pocket",9);
			if(health>39)A_SetTics(0);
		}
		#### AA 2 offset(0,42);
		#### A 2 offset(0,44) A_StartSound("weapons/pocket",9);
		#### A 4 offset(0,43);
		#### A 6 offset(0,42);
		#### A 8 offset(0,38)A_StartSound("weapons/maulload",8);
		#### A 4 offset(0,37){if(health>39)A_SetTics(0);}
		#### A 4 offset(0,36)A_StartSound("weapons/maulclose",8);

		#### A 0{
			let mmm=HDMagAmmo(findinventory("HDBattery"));
			if(mmm)invoker.weaponstatus[MLRS_BATTERY]=mmm.TakeMag(true);
		}goto reload3;

	reload3:
		#### A 6 offset(0,40) A_StartSound("weapons/maulclose2",8);
		#### A 2 offset(0,36);
		#### A 4 offset(0,33);
		goto nope;

	user3:
		#### A 0 A_MagManager("HDBattery");
		goto ready;

	spawn:
		MLRP A -1 nodelay{
			if(invoker.weaponstatus[0]&MLRF_CRITICAL)invoker.setstatelabel("bwahahahaha");
			else;
		}
	bwahahahaha:
		MLRP A 3{
			invoker.weaponstatus[MLRS_CRITTIMER]--;
			A_StartSound("weapons/maulcharge",CHAN_AUTO);
			BFG9k.Spark(self,1,6);
			if(invoker.weaponstatus[MLRS_CRITTIMER]<1){
				invoker.weaponstatus[MLRS_CRITTIMER]=0;
				invoker.setstatelabel("heh");
			}else if(invoker.weaponstatus[MLRS_CRITTIMER]<10)A_SetTics(2);
			else if(invoker.weaponstatus[MLRS_CRITTIMER]<5)A_SetTics(1);
		}wait;
	heh:
		MLRP A 8;
		MLRP A 4{
			invoker.A_StartSound("weapons/MaulFwoosh",CHAN_AUTO);
			invoker.weaponstatus[0]&=~MLRF_CRITICAL; //DO NOT DELETE THIS
			invoker.ShootBall(invoker,invoker.lastenemy);
		}
		MLRP A 0{
			invoker.A_ChangeVelocity(-cos(pitch)*4,0,sin(pitch)*4,CVF_RELATIVE);
		}goto spawn;
    }
	override void InitializeWepStats(){
		weaponstatus[MLRS_BATTERY]=20;
		weaponstatus[MLRS_TIMER]=0;
		weaponstatus[MLRS_CRITTIMER]=0;
		
	}
    override void loadoutconfigure(string input){
		int fm=getloadoutvar(input,"alt",1);
		if(!fm)weaponstatus[0]&=~MLRF_ALT;
		else if(fm>0)weaponstatus[0]|=MLRF_ALT;
	}


}

enum maulerstatus{
	MLRF_CRITICAL=1,
	MLRF_ALT=2,
	MLRF_JUSTUNLOAD=4,


	MLRS_STATUS=0,
	MLRS_CHARGE=1,
	MLRS_BATTERY=2,
	MLRS_TIMER=3,
	
	MLRS_CRITTIMER=4,

};

class MaulerBall:HDFireball{
	int ballripdmg;
	bool freedoom;
	default{
		-notelestomp +telestomp
		+skyexplode +forceradiusdmg +ripper -noteleport +notarget
		+bright
		decal "HDBFGLightning";
		renderstyle "add";
		damagefunction(ballripdmg);
		seesound "weapons/maulsf";
		deathsound "weapons/maultx";
		obituary "$OB_MPBFG_BOOM";
		alpha 0.9;
		height 4;
		radius 4;
		speed 4;
		gravity 0;
		scale 0.666;
	}
	
	void A_BFGBallSplodeZap(){
		blockthingsiterator it=blockthingsiterator.create(self,384);
		while(it.Next()){
			actor itt=it.thing;
			if(
				(itt.bismonster||itt.player)
				&&itt!=target
				&&itt.health>0
				&&!target.isfriend(itt)
				&&!target.isteammate(itt)
				&&checksight(itt)
			){
				A_Face(itt,0,0);
				int hhh=min(itt.health,4096);
				for(int i=0;i<hhh;i+=1024){
					A_CustomRailgun((0),0,"",freedoom?"55 88 ff":"55 ff 88",
						RGF_CENTERZ|RGF_SILENT|RGF_NOPIERCING|RGF_FULLBRIGHT,
						0,50.0,"BFGPuff",3,3,384,18,0.2,1.0
					);
				}
			}
		}
	}
	void A_BFGScrew(bool tail=false){
		A_Corkscrew();
		if(tail){
			let ttt=spawn("MaulerBallTail",pos,ALLOW_REPLACE);
			if(ttt){
				ttt.target=target;
				ttt.vel=vel*0.2;
			}
		}
	}
	states{
	spawn:
		TNT1 A 0 nodelay{
			//A_BFGSpray();
			ballripdmg=1;
			let hdp=hdplayerpawn(target);
			if(hdp){
				pitch=hdp.gunpitch;
				angle=hdp.gunangle;
			}else if(countinv("IsMoving",AAPTR_TARGET)>=6){
				pitch+=frandom(-3,3);
				angle+=frandom(-1,1);
			}
			freedoom=(Wads.CheckNumForName("FREEDOOM",0)!=-1);
		}
		BFS1 AB 2 A_SpawnItemEx("MaulerBallTail",0,0,0,vel.x*0.2,vel.y*0.2,vel.z*0.2,0,168,0);
		BFS1 A 0{
			ballripdmg=random(500,1000);
			bripper=false;
		}
		goto spawn2;
	spawn2:
		BFS1 AB 4 A_BFGScrew(true);
		loop;
	death:
		BFE1 A 2;
		BFE1 B 2 A_Explode(160,256,0);
		BFE1 B 4{
			DistantQuaker.Quake(self,
				6,100,16384,10,256,512,128
			);
			DistantNoise.Make(self,"world/maulfar");
		}
		TNT1 AAAAA 0 A_SpawnItemEx("HDSmokeChunk",random(-2,0),random(-3,3),random(-2,2),random(-5,0),random(-5,5),random(0,5),random(100,260),SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION,16);
		TNT1 AAAAA 0 A_SpawnItemEx("BFGBallRemains",-1,0,-12,0,0,0,SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION,16);
		BFE1 C 2 A_BFGBallSplodeZap();
		BFE1 CCC 2;
		BFE1 CCC 0 A_SpawnItemEx("HDSmoke",random(-4,0),random(-3,3),random(0,4),random(-1,1),random(-1,1),random(1,3),0,SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION,16);
		BFE1 DEF 6;
		BFE1 F 3 bright A_FadeOut(0.1);
		wait;
	}
}

class MaulerBallTail:IdleDummy{
	default{
		+forcexybillboard
		scale 0.5;renderstyle "add";
	}
	states{
	spawn:
		BFS1 AB 2 bright A_FadeOut(0.2);
		loop;
	}
}

class MaulerSpotFlash:IdleDummy{
	default{
		+puffonactors +hittracer +puffgetsowner +rollsprite +rollcenter +forcexybillboard
		renderstyle "add";
		obituary "%o was hammered by %k's particle splatter.";
		decal "Scorch";
		seesound "weapons/plasmaf";
		deathsound "weapons/plasmaf";
		translation "0:255=%[0.00,0.40,0.00]:[1.09,2.00,1.09]";
	}
	double impactdistance;
	override void postbeginplay(){
		if(impactdistance>2000){
			destroy();
			return;
		}
		super.postbeginplay();

		double impactcloseness=2000-impactdistance;
		scale*=(impactcloseness)*0.0006;
		alpha=scale.y+0.3;
		vel=(frandom(-1,1),frandom(-1,1),frandom(1,3));

		int n=int(max(impactcloseness*0.03,2));
		int n1=n*3/5;
		int n2=n*2/5;
		if(tracer){
			HDF.Give(tracer,"Heat",n);
			int dmgflags=target&&target.player?DMG_PLAYERATTACK:0;
			tracer.damagemobj(self,target,random(n1,n),"electrical",dmgflags);
		}
		A_HDBlast(
			n*2,random(1,n),n,"electrical",
			n,-n,
			immolateradius:n1,immolateamount:random(4,8)*n2/-10,immolatechance:n
		);

		pitch=frandom(80,90);
		angle=frandom(0,360);
		A_SpawnItemEx("MaulerSpotFlashLight",flags:SXF_NOCHECKPOSITION|SXF_SETTARGET);
		A_SpawnChunks("HDGunSmoke",clamp(n2*3/5,4,7),3,6);
		A_StartSound("weapons/maulx", CHAN_AUTO);
		A_AlertMonsters();
	}
	states{
	spawn:
		PLSE AB 1 bright;
		PLSE AAA 1 bright A_FadeIn(0.1);
		PLSE BCDE 1 bright A_FadeOut(0.1);
		stop;
	}
}

class MaulerSpotFlashLight:PointLight{
	override void postbeginplay(){
		super.postbeginplay();
		args[0]=128;
		args[1]=256;
		args[2]=96;
		args[3]=64;
		args[4]=0;
	}
	override void tick(){
		if(isfrozen())return;
		args[3]+=randompick(-10,5,-20);
		if(args[3]<1)destroy();
	}
}

//LINGERING THUNDER 2: THE SEQUEL: BECAUSE I NEED TO CHANGE THE COLOR
//OF THE DYNAMIC LIGHT AND THIS IS THE EASIEST WAY I CAN THINK OF!
//and also because im scared of zscript
class MaulerLingeringThunder:IdleDummy{
	int startingstamina;
	default{
		stamina 256;
	}
	override void postbeginplay(){
		super.postbeginplay();
		startingstamina=stamina;
	}
	void A_Zap(){
		if(stamina<1){destroy();return;}
		stamina-=5;
		blockthingsiterator zit=blockthingsiterator.create(self,96+(stamina>>2));
		int icount=0;
		bool haszapped=false;
		while(zit.next()){
			actor zt=zit.thing;
			if(
				!zt
				||(!zt.bshootable&&!zt.bsolid)
				||abs(zt.pos.z-pos.z)>96
				||zt.floorz+(stamina>>2)<zt.pos.z
				||random(0,3)
				||!checksight(zt)
			)continue;
			haszapped=true;
			int zappower=Zap(zt,self,target,stamina);
			stamina-=max(2,zappower>>3);
		}
		if(!haszapped){
			double oldrad=radius;
			a_setsize(stamina,height);
			Zap(self,self,target,stamina,true);
			a_setsize(oldrad,height);
		}
		A_SetTics(max(1,min(random(4,24),int(sqrt(startingstamina-stamina)))));
	}
	static int Zap(actor victim,actor inflictor,actor source,int baseamount,bool nodmg=false){
		//create arc
		double ztr=victim.radius;
		vector3 nodes[4];
		int len=min(35,baseamount);
		nodes[0]=victim.pos+(frandom(-ztr,ztr),frandom(-ztr,ztr),frandom(0,victim.height));
		nodes[1]=nodes[0]+(frandom(-len,len),frandom(-len,len),frandom(-len,len));
		nodes[2]=nodes[1]+(frandom(-len,len),frandom(-len,len),frandom(-(len>>1),len));
		nodes[3]=nodes[2]+(frandom(-len,len),frandom(-len,len),frandom(-len*2/3,(len>>1)));
		for(int i=1;i<4;i++){
			vector3 pastnode=nodes[i-1];
			vector3 particlepos=nodes[i]-pastnode;
			int iterations=int(particlepos.length());
			vector3 particlemove=particlepos/iterations;
			particlepos=pastnode-victim.pos;
			for(int i=0;i<iterations;i++){
				victim.A_SpawnParticle("white",
					SPF_RELATIVE|SPF_FULLBRIGHT,(len>>1),frandom(1,7),0,
					particlepos.x,particlepos.y,particlepos.z,
					frandom(-0.1,0.1),frandom(-0.1,0.1),frandom(0.1,0.2),
					frandom(-0.1,0.1),frandom(-0.1,0.1),-0.05
				);
				particlepos+=particlemove+(frandom(-1,1),frandom(-1,1),frandom(-1,1));
			}
		}

		int zappower=random(baseamount>>2,baseamount);
		victim.A_StartSound("weapons/plasidle",CHAN_AUTO,volume:frandom(0.2,0.6));
		victim.A_StartSound("misc/arccrackle",CHAN_AUTO);
		victim.A_StartSound("weapons/plascrack",CHAN_AUTO,volume:frandom(0.2,0.6));
		actor bsfl=spawn("MaulerSpotFlashLight",victim.pos,ALLOW_REPLACE);
		bsfl.target=victim;

		//make bodies spasm
		if(
			victim.bcorpse
			&&victim.bshootable
			&&victim.mass
			&&!!victim.findstate("dead")
		){
			victim.vel.z+=3.*zappower/victim.mass;
		}

		if(!nodmg)victim.damagemobj(inflictor,source,zappower,"electrical",source&&source.player?DMG_PLAYERATTACK:0);
		return zappower;
	}
	states{
	spawn:
		TNT1 A 1 A_Zap();
		wait;
	}
}
